import React, { useEffect, useContext } from 'react';
import {Row, Col} from "react-bootstrap";
import {ListGroupItem} from "react-bootstrap";
import ProductsContext from "../context/products-context";

const Product = ({ product }) => {
    const {dispatch} = useContext(ProductsContext);


    return (
        <ListGroupItem>
            <Row>
                <Col>
                    <h3>{product.title}</h3>
                    <p>{product.description}</p>
                </Col>
                <Col>
                    <button onClick={() => dispatch({type: "REMOVE_PRODUCT", id: product.id})}>x</button>
                </Col>
            </Row>
        </ListGroupItem>

    )
}

export default Product;