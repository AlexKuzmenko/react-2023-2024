import React, {useState, useContext} from "react";
import ProductsContext from "../context/products-context";

const AddProductForm = () => {
    const {dispatch} = useContext(ProductsContext);
    const [title, setTitle] = useState('')
    const [description, setDescription] = useState('')

    const addProduct = (e) => {
        e.preventDefault()
        dispatch({
            type: 'ADD_PRODUCT',
            title,
            description
        })
        setTitle('')
        setDescription('')
    }

    return (
        <div>
            <p>Add product</p>
            <form onSubmit={addProduct}>
                <input value={title} onChange={(e) => setTitle(e.target.value)} />
                <textarea value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
                <button>add product</button>
            </form>
        </div>
    )
}

export default AddProductForm;