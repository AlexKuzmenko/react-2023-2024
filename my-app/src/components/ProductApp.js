import React, { useEffect, useReducer } from 'react';
import ProductList from "./ProductList";
import AddProductForm from "./AddProductForm";
import productsReducer from "../reducers/products";
import ProductsContext from "../context/products-context";

import {Container} from "react-bootstrap";

const ProductApp = () => {
    const [products, dispatch] = useReducer(productsReducer, [])


    useEffect(() => {
        const productData = JSON.parse(localStorage.getItem('products'))
        if (productData) {
            console.log(productData);
            dispatch({ type: 'POPULATE_PRODUCTS', products: productData })
        }
    }, [])

    useEffect(() => {
        localStorage.setItem('products', JSON.stringify(products))
    }, [products])

    return (
        <ProductsContext.Provider value={{products, dispatch}}>
            <Container>
                <h1>Products</h1>
                <ProductList />
                <AddProductForm />

            </Container>
        </ProductsContext.Provider>

    )
}

export default ProductApp;