const productsReducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_PRODUCTS':
            return action.products
        case 'ADD_PRODUCT':
            let id = (state.length > 0 ) ? state[state.length - 1].id + 1 : 0;
            return [
                ...state,
                { id: id, title: action.title, description: action.description }
            ]
        case 'REMOVE_PRODUCT':
            return state.filter((product) => product.id !== action.id )
        default:
            return state
    }
}

export default productsReducer;