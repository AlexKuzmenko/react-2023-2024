import './App.css';
import ProductApp from "./components/ProductApp";
import {Container, Row, Col} from "react-bootstrap";


function App() {
  return (
      <Container>
        <Row className="justify-content-center">
          <Col sm={6}>
            <ProductApp />
          </Col>
        </Row>
      </Container>

  );
}

export default App;
