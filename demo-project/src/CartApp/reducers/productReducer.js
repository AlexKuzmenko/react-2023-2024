const productReducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_PRODUCTS':
            return action.productsData
        default: return state;
    }
}

export default productReducer;