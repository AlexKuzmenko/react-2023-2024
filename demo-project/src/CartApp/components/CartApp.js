import productReducer from "../reducers/productReducer";
import {Col, Container, Row} from "react-bootstrap";
import {useEffect, useReducer, useState} from "react";
import ProductCard from "./ProductCard";

const CartApp = () => {
    const [products, dispatch] = useReducer(productReducer, null)
    const [productsData, setProductsData] = useState([]); //Вхідні дані

    useEffect(() => {
        fetchData();
    }, [])

    useEffect(() => {
        dispatch({type: 'POPULATE_PRODUCTS', productsData})
    }, [productsData])

    const fetchData = async () => {
        const response = await fetch('https://fakestoreapi.com/products');
        const data = await response.json();
        setProductsData(data);
    }

    return <div>
        <Container>
            <h1>Products</h1>
            <Row>
                {products && products.map((product) => <Col xs={12} md={6} lg={3} >
                    <ProductCard product={product} />
                </Col>)}
            </Row>
        </Container>
    </div>
}

export default CartApp;