import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCoffee, faHeart} from "@fortawesome/free-solid-svg-icons";
import {useRef} from "react";

export default function ProductCard({product}) {
    const cartRef = useRef(null);

    const handleCartFocus = () => {
        cartRef.current.classList.toggle("text-danger");
    }

    return <div className="card" onClick={handleCartFocus}>
        <div className="d-flex justify-content-end p-2">
            <FontAwesomeIcon icon={faHeart} ref={cartRef} />
        </div>
        <div className="card-body">
            <h5 className="card-title">{product.title}</h5>
            <h6 className="card-subtitle mb-2 text-muted">{product.price}</h6>
            <p className="card-text">{product.description}</p>
            <a href="#" className="card-link">Card link</a>
            <a href="#" className="card-link">Another link</a>
        </div>
    </div>
}