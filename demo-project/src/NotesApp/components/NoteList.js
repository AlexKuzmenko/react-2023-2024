import Note from "./Note";
import NotesContext from "../context/NotesContext";
import {useContext} from "react";

export default function NoteList() {
    const {notes} = useContext(NotesContext)
    return <div>
        {notes.map((note) => <Note key={note.id} note={note} />)}
    </div>
}