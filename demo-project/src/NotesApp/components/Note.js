import useMousePosition from "../hooks/useMousePosition";
import {useContext} from "react";
import NotesContext from "../context/NotesContext";

export default function Note({note}) {

    const position = useMousePosition();
    const {dispatch} = useContext(NotesContext)

    return <div>
        <div>
            <h3>{note.title}</h3>
            <p>{note.description}</p>
            <p>{position.x} {position.y}</p>
        </div>
        <div>
            <button onClick={() => dispatch({type: "REMOVE_NOTE", id: note.id})}>X</button>
        </div>
    </div>
}