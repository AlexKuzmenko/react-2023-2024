import {useContext, useState} from "react";
import {v4 as uuidv4} from "uuid";
import NotesContext from "../context/NotesContext";

export default function NoteForm() {
    const [title, setTitle] = useState("")
    const [description, setDescription] = useState("")
    const {dispatch} = useContext(NotesContext);

    function addNote(e) {
        e.preventDefault();
        dispatch({type: "ADD_NOTE", title, description})
        setTitle("")
        setDescription("")
    }

    return <form onSubmit={addNote}>
        <input type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
        <textarea value={description} onChange={(e) => setDescription(e.target.value)}></textarea>
        <button type="submit">OK</button>
    </form>
}