import {useEffect, useReducer, useState} from "react";
import {v4 as uuidv4} from "uuid";
import NoteList from "./NoteList";
import NoteForm from "./NoteForm";
import notesReducer from "../reducers/notes-reducer";
import NotesContext from "../context/NotesContext";

function NotesApp() {
    const [notes, dispatch] = useReducer(notesReducer, [])

    useEffect(() => {
        const notesData = JSON.parse(localStorage.getItem("notes"));
        if (notesData) {
            dispatch({type: "POPULATE_NOTES", notes: notesData})
        }
    }, []);

    useEffect(() => {
        localStorage.setItem("notes", JSON.stringify(notes))
    }, [notes]);

    return <NotesContext.Provider value={{notes, dispatch}} >
        <div>
            <div>
                <NoteList />
            </div>
            <div>
                <NoteForm />
            </div>
        </div>
    </NotesContext.Provider>


}

export default NotesApp;