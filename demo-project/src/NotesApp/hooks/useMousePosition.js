import {useEffect, useState} from "react";

const useMousePosition = () => {
    //Створюємо стан з координатами курсора
    const [position, setPosition] = useState({x: 0, y: 0})

    //При першому рендерингу компонента, обробляємо подію руху курсора
    useEffect(() => {
        console.log("Control")
        const handleMouseMove = (e) => {
            setPosition({
                x: e.pageX,
                y: e.pageY
            })
        }
        document.addEventListener("mousemove", handleMouseMove)
        //При видаленні компонента, необхідно видалити обробку події
        return () => {
            console.log("removeComponent")
            document.removeEventListener("mousemove", handleMouseMove)
        }
    }, []);
    //Повертаємо стан
    return position;
}

export default useMousePosition;