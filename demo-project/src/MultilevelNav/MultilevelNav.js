import React, {useRef, useState} from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCaretDown, faCaretRight} from "@fortawesome/free-solid-svg-icons";


function ListItem({ item, index }) {
    const subListRef = useRef(null);
    const [openSubList, setOpenSubList] = useState(false);
    const toggleSubList = () => {
        subListRef.current.classList.toggle("open");
        setOpenSubList(!openSubList);
    }
    return (
        <li key={index} className="border border-1 border-warning m-1">
            <div className="d-flex justify-content-between">
                <div>{item.title}</div>
                {item.subitems && <div>
                    <FontAwesomeIcon icon={openSubList ? faCaretDown : faCaretRight} onClick={toggleSubList} />
                </div>}

            </div>
            {item.subitems && <ul ref={subListRef} className="nested-list ms-3">
                {item.subitems.map((subitem, subindex) => (
                    <ListItem item={subitem} index={subindex} />
                ))}
            </ul>}

        </li>
    )
}

function MultilevelNav({ data }) {
    return (
        <ul className="border border-3 border-info m-3 multilevel-accordion-list">
            {data.map((item, index) => (
                <ListItem item={item} index={index} />
            ))}
        </ul>
    );
}

export default MultilevelNav;