import localforage from "localforage";

export async function getUsers() {
    let users = await localforage.getItem("users");
    if (!users) users = [];
    return users;
}

export async function createUser({ username, age }) {
    let id = Math.random().toString(36).substring(2, 9);
    let user = { id, username, age };
    let users = await getUsers();
    users.push(user);
    await set(users);
    return user;
}

export async function getUser(id) {
    let users = await localforage.getItem("users");
    let user = users.find((user) => user.id === id);
    return user ?? null;
}

export async function deleteUser(id) {
    let users = await localforage.getItem("users");
    let length1 = users.length;
    users = users.filter((user) => user.id !== id);
    let length2 = users.length;
    if (length2 < length1) {
        await set(users);
        return true;
    }
    return false;
}

function set(users) {
    return localforage.setItem("users", users);
}