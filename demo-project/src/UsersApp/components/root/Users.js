import {Col, Container, Row} from "react-bootstrap";
import {Link, Outlet, useLoaderData} from "react-router-dom";
import {getUsers} from "../../models/users";

export async function loader() {
    return getUsers();
}

export default function Users() {
    const users = useLoaderData();

    return <Container>
        <h2>Users</h2>
        <Row>
            <Col xs={3}>
                <Link className="btn btn-primary" to="form">New User</Link>

                <div className="list-group mt-3">
                    {users.map((user) => <Link
                        key={user.id}
                        to={`${user.id}`}
                        className="list-group-item"
                    >
                        {user.username}
                    </Link>)}
                </div>
            </Col>
            <Col xs={9} className="border border-2 border-success p-3">
                <Outlet />
            </Col>
        </Row>
    </Container>
}