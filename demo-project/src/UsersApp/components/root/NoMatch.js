import {Link} from "react-router-dom";

export default function NoMatch() {
    return <div>
        <h2>NoMatch</h2>
        <Link to="/">Dashboard</Link>
    </div>
}