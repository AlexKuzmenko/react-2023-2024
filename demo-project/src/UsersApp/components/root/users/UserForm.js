import {Form, redirect} from "react-router-dom";
import {createUser} from "../../../models/users";

export default function UserForm() {
    return <div>
        <h3>User Form</h3>
        <Form method="post">
            <p>
                <label>
                    Username
                    <br />
                    <input type="text" name="username" />
                </label>
            </p>
            <p>
                <label>
                    Age
                    <br />
                    <input type="number" name="age" />
                </label>
            </p>
            <p>
                <button type="submit">Save</button>
            </p>
        </Form>
    </div>
}

export async function action({ request }) {
    const formData = await request.formData();
    const user = await createUser({
        username: formData.get("username"),
        age: formData.get("age"),
    });
    return redirect(`/users/${user.id}`);
}