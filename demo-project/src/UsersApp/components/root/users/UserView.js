import {Form, redirect, useLoaderData} from "react-router-dom";
import {deleteUser, getUser} from "../../../models/users";

export async function loader({ params }) {
    const user = await getUser(params.userId);
    if (!user) throw new Response("", { status: 404 });
    return user;
}

export async function action({ params }) {
    await deleteUser(params.userId);
    return redirect("/users");
}

export default function UserView() {
    const user = useLoaderData();
    return <div>
        <h3>User View</h3>
        <div className="list-group">
            {user && <div className="list-group-item">Username: {user.username}</div>}
        </div>
        <Form method="post" style={{ marginTop: "2rem" }}>
            <button type="submit">Delete</button>
        </Form>
    </div>
}