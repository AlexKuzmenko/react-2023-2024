import {Link, Outlet} from "react-router-dom";
import {Col, Container, Row} from "react-bootstrap";

export default function Root() {
    return <Container className="border border-2 border-info p-3 m-3">
        <h1>UsersApp</h1>
        <ul className="nav justify-content-center">
            <li className="nav-item">
                <Link className="nav-link" to="/">Dashboard</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="contact">Contact</Link>
            </li>
            <li className="nav-item">
                <Link className="nav-link" to="users">Users</Link>
            </li>
        </ul>
        <div className="p-3 border border-3 border-warning">
            <Outlet/>
        </div>
    </Container>
}