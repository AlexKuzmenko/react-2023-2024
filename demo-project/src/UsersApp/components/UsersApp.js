import React from "react";
import {
    createBrowserRouter,
    RouterProvider,
    createRoutesFromElements, Route,
} from "react-router-dom";

import Root from "./Root";
import Dashboard from "./root/Dashboard";
import NoMatch from "./root/NoMatch";
import Contacts from "./root/Contacts";
import Users, {loader as usersLoader} from "./root/Users";
import UserView, {loader as userLoader, action as userAction} from "./root/users/UserView";
import UserForm, {action as newUserAction} from "./root/users/UserForm";



export default function UsersApp() {
    // Configure nested routes with JSX
    // const router = createBrowserRouter(
    //     createRoutesFromElements(
    //         <Route path="/" element={<Root/>}>
    //             <Route path="/" element={<Dashboard />} />
    //             <Route path="contact" element={<Contact />} />
    //             <Route path="users" element={<Users />}>
    //                 <Route path=":userId" element={<UserView />} />
    //                 <Route path="form" element={<UserForm />} />
    //             </Route>
    //             <Route path="*" element={<NoMatch />} />
    //         </Route>
    //     )
    // );
    const router = createBrowserRouter([
        {
            element: <Root />,
            path: "/",
            children: [
                {
                    element: <Dashboard />,
                    path: "/",
                },
                {
                    element: <Contacts />,
                    path: "contact",
                },

                {
                    element: <NoMatch />,
                    path: "*",
                },
                {
                    element: <Users />,
                    path: "users",
                    loader: usersLoader,
                    children: [
                        {
                            element: <UserView />,
                            path: ":userId",
                            loader: userLoader,
                            action: userAction,
                        },
                        {
                            element: <UserForm />,
                            path: "form",
                            action: newUserAction,
                        }
                    ]
                },
            ]
        }
    ])

    return <RouterProvider router={router} />

}