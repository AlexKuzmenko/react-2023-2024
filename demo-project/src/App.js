import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import Counter from "./DemoApp/components/Counter";
import NotesApp from "./NotesApp/components/NotesApp";
import ProductApp from "./ProductApp/components/ProductApp";
import UsersApp from "./UsersApp/components/UsersApp";
import NovaPoshtaApp from "./NovaPoshtaApp/NovaPoshtaApp";
import CartApp from "./CartApp/components/CartApp";
import MultilevelNav from "./MultilevelNav/MultilevelNav";
import FormDemo from "./NovaPoshtaApp/FormDemo";



function App() {

    const categories = [
        {
            title: "Electronics",
            subitems: [
                {
                    title: "Smartphones",
                    subitems: [
                        {
                            title: "Apple",
                            subitems: [
                                {
                                    title: "iPhone 13",
                                },
                                {
                                    title: "iPhone 12",
                                }
                            ]
                        },
                        {
                            title: "Samsung",
                            subitems: [
                                {
                                    title: "Galaxy S21",
                                },
                                {
                                    title: "Galaxy A52",
                                }
                            ]
                        }
                    ]
                },
                {
                    title: "Laptops",
                    subitems: [
                        {
                            title: "Apple",
                            subitems: [
                                {
                                    title: "MacBook Pro",
                                },
                                {
                                    title: "MacBook Air",
                                }
                            ]
                        },
                        {
                            title: "Dell",
                            subitems: [
                                {
                                    title: "XPS 13",
                                },
                                {
                                    title: "Inspiron 15",
                                }
                            ]
                        }
                    ]
                }
            ]
        },
        {
            title: "Clothing",
            subitems: [
                {
                    title: "Men's",
                },
                {
                    title: "Women's",
                }
            ]
        }
    ];
  return (
    <div classtitle="App">
        <main>
            <FormDemo />
        </main>
    </div>
  );
}

export default App;
