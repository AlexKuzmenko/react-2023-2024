import ProductList from "./ProductList";
import Home from "./Home";
import React from "react";
import {
    BrowserRouter as Router,
    Routes,
    Route,
    Link, createBrowserRouter, RouterProvider, Outlet
} from "react-router-dom";
import ProductRoot from "./ProductRoot";



export default function ProductApp() {

    const router = createBrowserRouter([
        {
            element: <ProductRoot />,
            path: "/",
            children: [
                {
                    element: <Home/>,
                    path: "",
                },
                {
                    element: <ProductList/>,
                    path: "products"
                }
            ]
        },

    ])

    return <RouterProvider router={router}></RouterProvider>
}