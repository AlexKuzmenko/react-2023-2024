import {Link, Outlet} from "react-router-dom";
import React from "react";

export default function ProductRoot()
{
    return <div className="border border-4 border-info">
        <nav>
            <ul>
                <li>
                    <Link to="/">Home</Link>
                </li>
                <li>
                    <Link to="/products">Products</Link>
                </li>
            </ul>
        </nav>

        <div>
            <h1>Users Application</h1>
            <div className="border border-3 border-warning">
                <Outlet />
            </div>

        </div>
    </div>
}