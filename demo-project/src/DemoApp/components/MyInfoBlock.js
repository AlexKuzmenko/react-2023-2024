export default function MyInfoBlock() {
    const myApp = {
        title: "MyApp",
        subTitle: "MyApp Description",
    }
    return <div>
            <h1>{myApp.title}</h1>
            <p>{myApp.subTitle}</p>
        </div>;
}