import {Button} from "react-bootstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCoffee, faPlus, faMinus} from "@fortawesome/free-solid-svg-icons";
import {useState} from "react";

function Counter({id, initialValue, min, max}) {
    const [count, setCount] = useState(initialValue);

    const addCount = () => {
        if (count < max) {
            setCount(count + 1)
        }
    }

    return <div className="m-4">
        <h2>{count}</h2>
        <Button className="btn-info" onClick={addCount}>
            <FontAwesomeIcon icon={faPlus} />
        </Button>
        <Button className="btn-info" onClick={() => setCount(count - 1)}>
            <FontAwesomeIcon icon={faMinus} />
        </Button>
        <Button className="btn-info" onClick={() => setCount(initialValue)}>
            Reset
        </Button>
    </div>
}

Counter.defaultProps = {
    initialValue: 0,
    min: -10,
    max: 10,
}

export default Counter;