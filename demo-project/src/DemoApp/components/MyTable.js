export default function MyTable () {
    return (
        <table className="my-table">
            <tr>
                <td>Name</td>
                <td>LastName</td>
            </tr>
            <tr>
                <td>John</td>
                <td>Smith</td>
            </tr>
        </table>
    );
}