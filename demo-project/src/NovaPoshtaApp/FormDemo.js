import { useForm } from "react-hook-form"
import React, {useState} from "react";

function FormCargo() {
    const {
        register,
        formState: { errors },
    } = useForm()
    return <>
        <div>
            Довжина <input {...register("length")} />
        </div>
        <div>
            Ширина <input {...register("width")} />
        </div>
        <div>
            Висота <input {...register("hight")} />
        </div>
    </>
}

function FormPalletes() {
    const {
        register,
        formState: { errors },
    } = useForm()
    return <>
        <div>
            Тип <input {...register("type")} />
        </div>
        <div>
            Оголошена вартість <input {...register("declaredPrice")} />
        </div>
        <div>
            Кількість <input {...register("quantity")} />
        </div>

    </>
}

export default function FormDemo() {

    const [shipmentType, setShipmentType] = useState('cargo');
    function handleShipmentTypeChange(e) {
        setShipmentType(e.target.value);
    }


    const {
        register,
        handleSubmit,
        watch,
        formState: { errors },
    } = useForm()

    const onSubmit = (data) => console.log(data)

    console.log(watch("example")) // watch input value by passing the name of it

    return (
        /* "handleSubmit" will validate your inputs before invoking "onSubmit" */
        <form onSubmit={handleSubmit(onSubmit)}>
            {/* register your input into the hook by invoking the "register" function */}
            <input defaultValue="test" {...register("example")} />

            {/* include validation with required or other standard HTML validation rules */}
            <input {...register("exampleRequired", { required: true })} />
            {/* errors will return when field validation fails  */}
            {errors.exampleRequired && <span>This field is required</span>}

            <select {...register("shipmentType")} onChange={handleShipmentTypeChange}>
                <option value="cargo">Cargo</option>
                <option value="palletes">Pallets</option>
            </select>

            {shipmentType == 'cargo' && <FormCargo />}
            {shipmentType == 'palletes' && <FormPalletes />}

            <input type="submit" />
        </form>
    )
}