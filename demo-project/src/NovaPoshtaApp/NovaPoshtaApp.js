import {useForm} from "react-hook-form";
import React, {useRef} from "react";

// you can use React.forwardRef to pass the ref too
const Input = React.forwardRef(({ name, label }, ref) => (
    <>
        <label>{label}</label>
        <input name={name} ref={ref} />
    </>
))

export default function NovaPoshtaApp() {
    const {register, handleSubmit} = useForm();
    const onSubmit = data => console.log(data);
    const onError = data => console.log(data);

    const ref = useRef(null);

    function handleClick() {
        ref.current.focus();
    }

    function checkStudyYear(value) {
        return (value >= 1 && value <= 11)
    }

    return (
        <form onSubmit={handleSubmit(onSubmit, onError)}>
            <input {...register("firstName")} />
            <select {...register("gender")}>
                <option value="female">female</option>
                <option value="male">male</option>
                <option value="other">other</option>
            </select>
            <Input type="number" label="Age" {...register("Age")} ref={ref} />
            <input type="checkbox" {...register("language")} value="Ukrainian"/>
            <input type="checkbox" {...register("language")} value="English"/>
            <input type="checkbox" {...register("language")} value="Spanish"/>

            <input type="radio" {...register("delivery")} value="NovaPoshta"/>
            <input type="radio" {...register("delivery")} value="Ukrposhta" />

            <button type="button" onClick={handleClick}>
                Edit
            </button>

            <input type="number" {...register("studyYear", {validate: checkStudyYear})} />

            <input type="submit"/>
        </form>
    );
}