import React, { useEffect, useRef } from 'react';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faHeart} from "@fortawesome/free-solid-svg-icons";
import {faCartShopping} from "@fortawesome/free-solid-svg-icons";

const Product = ({ product }) => {

    const cartRef = useRef();

    useEffect(() => {
        console.log('Setting up effect!')

        return () => {
            console.log('Cleaning up effect!')
        }
    }, [])

    const handleCartFocus = () => {
        cartRef.current.classList.toggle('text-danger');
        console.log(cartRef);
    }

    return (
        <div className="card" onClick={handleCartFocus}>
            <div className="d-flex justify-content-end p-2">
                <FontAwesomeIcon icon={faCartShopping} ref={cartRef}  size="xl" />
            </div>


            <div className="card-body">
                <h4 className="card-title">{product.title}</h4>
            </div>
        </div>)
}

export default Product;