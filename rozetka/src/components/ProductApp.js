import React, { useState, useEffect, useReducer } from 'react';
import Product from "./Product";
import {Row,  Col, ListGroup} from "react-bootstrap";
import {Container} from "react-bootstrap";

const initialState = {products: []}

const productsReducer = (state, action) => {
    switch (action.type) {
        case 'POPULATE_PRODUCTS':
            return {products: action.productsData}
        case 'SORT_PRICE_ASC':
            return {products: action.productsData.slice().sort((a, b) => a.price - b.price)};
        case 'SORT_PRICE_DESC':
            return {products: action.productsData.slice().sort((a, b) => b.price - a.price)};

        default:
            return state
    }
}

const ProductApp = () => {

    const [state, dispatch] = useReducer(productsReducer, initialState);

    const [productsData, setProductsData] = useState([]);

    useEffect(() => {
        fetchProductsData();
    }, [])

    useEffect(() => {
        dispatch({ type: 'POPULATE_PRODUCTS', productsData })
    }, [productsData]);

    const fetchProductsData = async () => {
        let response = await fetch('https://fakestoreapi.com/products');
        let data = await response.json();
        setProductsData(data);

    }

    const productSortHandler = (event) => {
        let val = event.target.value;
        switch(val) {
            case "PRICE_ASC": dispatch({ type: 'SORT_PRICE_ASC', productsData}); break;
            case "PRICE_DESC": dispatch({ type: 'SORT_PRICE_DESC', productsData }); break;
        }
    }

    return (
        <Container>
            <h1>Products</h1>
            <div>
                <select name="" id="" onChange={productSortHandler}>
                    <option value="PRICE_ASC">Від дешевих до дорогих</option>
                    <option value="PRICE_DESC">Від дорогих до дешевих</option>
                </select>
            </div>

            <Row>
                {state.products && state.products.map((product) => (
                    <Col xs={12} md={6} lg={3} className="pb-4">
                        <Product key={product.id} product={product} />
                    </Col>
                ))}
            </Row>
        </Container>
    )
}

export default ProductApp;