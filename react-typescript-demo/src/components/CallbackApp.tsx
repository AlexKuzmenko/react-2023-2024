import {useCallback, useEffect, useMemo, useState} from "react";

interface ListProps {
    getItems: () => number[];
}

function List({getItems}: ListProps) {
    const [items, setItems] = useState<Array<number>>([]);

    useEffect(() => {
        setItems(getItems())
        console.log('Updating Items')
    }, [getItems]);

    return <div>
        {items.map((item, index) => <div key={index}>
            {item}
        </div>)}
    </div>

}

export default function CallbackApp() {
    const [number, setNumber] = useState<number>(0);
    const [dark, setDark] = useState(false);

    const getItems = useCallback((): number[] => {
        return [number, number + 1, number + 2];
    }, [number]);

    const theme = {
        backgroundColor: dark ? "black" : "white",
        color: dark ? "white" : "black"
    }

    return (
        <div style={theme}>
            <input
                type="number"
                value={number}
                onChange={e => setNumber(parseInt(e.target.value))}
            />
            <button onClick={() => setDark(prevDark => !prevDark)}>Change Theme</button>
            <List getItems={getItems}/>
        </div>
    )
}
