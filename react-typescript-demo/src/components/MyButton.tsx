import {useState} from "react";

interface MyButtonProps {
    title: string;
    disabled: boolean;
}

export default function MyButton({title}: MyButtonProps) {

    //Union-тип
    type Message = | {type: "success", data: any}
        | {type: "info", data: any}
        | {type: "warning", data: any}
        | {type: "danger", data: any};

    const [message, setMessage] = useState<Message>({
        type: "success",
        data: {text: "Successfully created"}
    });

    return (<div>

        </div>

    );
}