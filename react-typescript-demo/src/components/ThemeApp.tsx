import { createContext, useContext, useState } from 'react';

type Theme = "light" | "dark" | "system";
const ThemeContext = createContext<Theme | null>(null);

export default function ThemeApp() {
    const [theme, setTheme] = useState<Theme>("light");

    return (
        <ThemeContext.Provider value={theme}>
            <MyComponent />
        </ThemeContext.Provider>
    )
}

function MyComponent() {
    const theme = useContext(ThemeContext);

    return (
        <div>
            <p>Current theme: {theme}</p>
        </div>
    )
}