import {useEffect, useMemo, useState} from "react";

export default function MemoApp() {
    const [number, setNumber] = useState<number>(0);
    const [dark, setDark] = useState(false);
    const doubleNumber = useMemo(() => slowFunction(number), [number]);

    const theme = useMemo(() => {
        return {
            backgroundColor: dark ? "black" : "white",
            color: dark ? "white" : "black"
        };
    }, [dark]);




    useEffect(() => {
        console.log("Theme Changed")
    }, [theme]);

    return (
        <>
            <input type="number" value={number} onChange={e => setNumber(parseInt(e.target.value))}/>
            <button onClick={() => setDark(prevDark => !prevDark)}>Change Theme</button>
            <div style={theme}>{doubleNumber}</div>
        </>
    )
}

function slowFunction(n: number): number {
    console.log('Calling slow function');
    for (let i = 0; i <= 1000000000; i++) {
    }
    return n * 2;
}