import {useReducer} from 'react';
import productReducer, {State} from "../reducers/productReducer";

const initialState: State = { count: 0 };

export default function ProductApp() {
    const [state, dispatch] = useReducer(productReducer, initialState);

    const addFive = () => dispatch({ type: "setCount", value: state.count + 5 });
    const reset = () => dispatch({ type: "reset" });

    return (
        <div>
            <h1>Welcome to my counter</h1>

            <p>Count: {state.count}</p>
            <button onClick={addFive}>Add 5</button>
            <button onClick={reset}>Reset</button>
        </div>
    );
}
