export interface State {
    count: number
};

type CounterAction =
    | { type: "reset" }
    | { type: "setCount"; value: State["count"] }
    | { type: "setCountToValue"; value: State["count"] }


const initialState: State = { count: 0 };

function productReducer(state: State, action: CounterAction): State {
    switch (action.type) {
        case "setCount":
            return { ...state, count: action.value };
        case "reset":
            return initialState;
        default:
            throw new Error("Unknown action");
    }
}

export default productReducer;