import React from 'react';
import logo from './logo.svg';
import './App.css';
import MyButton from "./components/MyButton";
import ProductApp from "./components/ProductApp";
import ThemeApp from "./components/ThemeApp";
import MemoApp from "./components/MemoApp";
import CallbackApp from "./components/CallbackApp";

function App() {
  return (
    <div className="App">
      <MemoApp />
    </div>
  );
}

export default App;
