import type { Metadata } from 'next'
import Link from "next/link";
import {Col, Container, Row} from "react-bootstrap";
import Image from 'next/image';
import productImage from './product.jpg';

export default function CatalogLayout({
                                        children,
                                    }: {
    children: React.ReactNode
}) {
    return (
        <Container>
            <Row>
                <Col xs={12} md={4}>
                    <Image
                        src={productImage}
                        alt="Product"
                        // width={500} automatically provided
                        // height={500} automatically provided
                        // blurDataURL="data:..." automatically provided
                        // placeholder="blur" // Optional blur-up while loading
                    />
                </Col>
                <Col xs={12} md={6}>
                    {/*{children}*/}
                </Col>
            </Row>
        </Container>



    )
}
