import Link from "next/link";
import React from "react";
import Image from 'next/image';

interface Product {
    id: number,
    title: string,
    price: number,
    image: string
}

async function getProducts() {
    const response = await fetch(`https://fakestoreapi.com/products`, {
        next: {
            revalidate: 60
        }
    });
    if (!response.ok) throw new Error("Couldn't get products")
    const products: Product[] = await response.json();
    return products;
}

export default async function Products() {
    const products = await getProducts();

    return (
        <div>
            <h1>Products Page</h1>
            <table className="tw-table w-100">
                <thead className="tw-bg-gray-800 tw-text-white">
                <tr>
                    <th>Image</th>
                    <th>Title</th>
                    <th>Price</th>
                </tr>
                </thead>
                <tbody>
                {products.map((product: Product) => (
                    <tr key={product.id}>
                        <td>
                            <Image
                                src={product.image}
                                alt="Product picture"
                                width={500}
                                height={500}
                            />
                        </td>
                        <td>
                            {product.title}
                        </td>
                        <td>
                            {product.price}
                        </td>
                    </tr>
                ))}
                </tbody>

            </table>
        </div>
    )
}