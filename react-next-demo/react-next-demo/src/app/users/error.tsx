"use client";

export default function Error({ error }: {error: Error}) {
    return <h1>You have an error: {error.message}</h1>
}