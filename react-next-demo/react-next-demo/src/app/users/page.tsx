import Link from "next/link";

interface User {
    id: number,
    username: string,
    email: string
}

async function getUsers() {
    const response = await fetch(`https://jsonplaceholder.typicode.com/users`, {
        next: {
            revalidate: 60
        }
    });
    if (!response.ok) throw new Error("Couldn't get users")
    const users: User[] = await response.json();
    return users;
}

export default async function Users() {
    const users = await getUsers();

    return (
        <div className="container p-2">
            <h1>Users Page</h1>
            <button className="tw-btn tw-btn-active tw-btn-primary">Button</button>
            <table className="tw-table">
                <thead className="tw-bg-gray-800 tw-text-white">
                <tr>
                    <th>Username</th>
                    <th>Email</th>
                </tr>
                </thead>
                <tbody>
                {users.map((user: User) => (
                    <tr key={user.id}>
                        <td>
                            <Link href={`/users/${user.id}`}>
                                {user.username}
                            </Link>
                        </td>
                        <td>
                            {user.email}
                        </td>
                    </tr>
                ))}
                </tbody>

            </table>
        </div>
    )
}