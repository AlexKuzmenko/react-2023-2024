import {Metadata} from "next";
import {allowedDisplayValues} from "next/dist/compiled/@next/font/dist/constants";

interface Props {
    params: {id: number}
}

export async function generateMetadata({params: {id}}: Props): Promise<Metadata> {
    const user = await getUser(id);
    return {
        title: user.username
    }
}

async function getUser(id: number) {
    const response = await fetch(`https://jsonplaceholder.typicode.com/users/${id}`, {
        next: {
            revalidate: 60
        }
    });
    const user = await response.json();
    if (!response.ok) throw new Error("User not found");
    return user;
}

export default async function UserView({params: {id}}: Props) {
    const user = await getUser(id);
    return (
        <>
            <h1>User View {id}</h1>
            <p>{user.username}</p>
            <p>{user.email}</p>
        </>


    )
}