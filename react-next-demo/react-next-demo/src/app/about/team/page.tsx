import {Metadata} from "next";

export const metadata: Metadata = {
    title: 'About page: Team',
    description: 'About page: Team',
}

export default function AboutTeam() {
    return (
        <h1>About Page: Our Team</h1>
    )
}