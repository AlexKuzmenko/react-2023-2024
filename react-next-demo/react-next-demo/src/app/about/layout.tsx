import type { Metadata } from 'next'
import Link from "next/link";
import {Col, Container, Row} from "react-bootstrap";

export default function AboutLayout({
                                       children,
                                   }: {
    children: React.ReactNode
}) {
    return (
        <div className="container">
            <Row>
                <Col>
                    <nav className="list-group">
                        <Link className="list-group-item" href="/about">General info</Link>
                        <Link className="list-group-item" href="/about/contacts">Contacts</Link>
                        <Link className="list-group-item" href="/about/team">Team</Link>
                    </nav>
                </Col>
                <Col>
                    {children}
                </Col>
            </Row>
        </div>



    )
}
