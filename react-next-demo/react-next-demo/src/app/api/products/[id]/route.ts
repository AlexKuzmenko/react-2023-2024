import {NextResponse} from "next/server";

import { headers, cookies} from "next/headers";
import { redirect } from "next/navigation";

export async function GET(req: Request, {params}: { params: { id: string } }) {
    const id = params.id;
    const headersList = headers();
    const cookieList = cookies();

    let type = headersList.get("Content-Type");
    let cookie = cookieList.get("Cookie1")?.value;

    console.log(type);
    console.log(cookie);

    const response = await fetch(`https://fakestoreapi.com/products/${id}`);
    const product = await response.json();

    return NextResponse.json({product});
}

export async function DELETE(req: Request, {params}: { params: { id: string } }) {
    const id = params.id;

    const response = await fetch(`https://fakestoreapi.com/products/${id}`, {
        method: "DELETE"
    });
    const result = await response.json();
    return NextResponse.json({result});
}