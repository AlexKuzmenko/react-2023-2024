import {NextResponse} from "next/server";

export async function GET(req: Request) {
    const {searchParams} = new URL(req.url)
    const q = searchParams.get("q")

    const response = await fetch("https://fakestoreapi.com/products");
    const products = await response.json();

    let currentProducts = products;

    if (q) {
        currentProducts = products.filter((product:any) => product.title.includes(q))
    }


    return NextResponse.json(currentProducts)
}


export async function POST(req: Request) {
    const body = await req.json();

    //post data to database

    return NextResponse.json({body})
}