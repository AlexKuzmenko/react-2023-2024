import NextAuth from "next-auth";
import {authConfig} from "@/configs/auth";

const handler = NextAuth(authConfig);

//експортуємо обробник в двох варіантах
export {handler as GET, handler as POST};