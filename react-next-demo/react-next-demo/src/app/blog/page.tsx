"use client";

import {useEffect, useState} from "react";
import {getAllPosts} from "@/services/getPosts";
import Posts from "@/components/Posts/Posts";
import {PostSearch} from "@/components/PostSearch/PostSearch";
import {Container} from "react-bootstrap";

export default function Blog() {
    const [posts, setPosts] = useState<any[]>([])
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        getAllPosts()
            .then(setPosts)
            .finally(() => {
                setLoading(false);
            })
    }, [])

    return (
        <Container>
            <h1>Blog page</h1>
            <PostSearch onSearch={setPosts} />
            {loading ? (
                <h3>Loading ...</h3>
            ) : (
                <Posts posts={posts}/>
            )}
        </Container>
    )
}