import Link from "next/link";
import styles from "./Posts.module.css";


type Props = {
    posts: any[];
}

export default function Posts({posts}: Props) {
    return (
        <>
            <ul className={styles.list}>
                {posts.map((post: any) => (
                    <li className={styles.listItem} key={post.id}>
                        <Link href={`/blog/${post.id}`}>{post.title}</Link>
                    </li>
                ))}
            </ul>
        </>
    )
}