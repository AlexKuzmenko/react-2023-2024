"use client";
import Link from "next/link";
import {usePathname} from "next/navigation";
import {signOut, useSession} from "next-auth/react";

type NavLink = {
    label: string;
    href: string;
}
type Props = {
    navLinks: NavLink[];
}

const Navigation = ({navLinks}: Props) => {
    const pathName = usePathname();
    const session = useSession();

    console.log(session);

    return <nav>
        { navLinks.map(link => {
            const isActive = pathName === link.href;
            return <Link
                key={link.label}
                href={link.href}
                className={isActive ? 'active' : ""}
            >
                {link.label}
            </Link>
        }) }


        {session?.data && (
            <Link href="/profile">Profile</Link>
        )}
        {session?.data ?
            <Link href="#" onClick={() => signOut({
                callbackUrl: "/"
            })}>Sign Out</Link>
            :
            <Link href="/signin">Sign In</Link>}
    </nav>
}

export {Navigation}