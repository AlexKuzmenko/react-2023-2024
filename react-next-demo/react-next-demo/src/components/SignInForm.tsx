"use client";

import {useRouter} from "next/navigation";
import {FormEventHandler} from "react";
import {signIn} from "next-auth/react";

export const SignInForm = () => {
    const router = useRouter();

    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();
        const formData = new FormData(event.currentTarget);
        const res = await signIn("credentials", {
            email: formData.get("email"),
            password: formData.get("password"),
            redirect: false, //не перенаправити запит на форму по замовчуванню /api/auth/signin
        })

        if (res && !res.error) {
            router.push("/profile")
        } else {
            console.log(res);
        }
    }

    return (
        <form onSubmit={handleSubmit}>
            <input type="email" name="email" required />
            <input type="password" name="password" required />
            <button type="submit">Sign In</button>
        </form>
    )
}