"use client";

import {signIn} from "next-auth/react";
import {useSearchParams} from "next/navigation";

export const GoogleButton = () => {
    const searchParams = useSearchParams();
    const callbackUrl = searchParams.get("callbackUrl") || "/profile";
    return (
        <button className="p-2 bg-amber-500 text-white text-xl hover:bg-amber-600" onClick={() => signIn("google", {
            callbackUrl
        })}>
            Sign in with Google
        </button>
    )
}