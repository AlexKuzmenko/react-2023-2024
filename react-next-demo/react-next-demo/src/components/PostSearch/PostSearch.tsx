
import {FormEventHandler, useState} from "react";
import {getPostsBySearch} from "@/services/getPosts";
import styles from "./PostSearch.module.css";

type Props = {
    onSearch: (value: any[]) => void;
}

const PostSearch = ({onSearch}: Props) => {
    const [search, setSearch] = useState("")
    const handleSubmit: FormEventHandler<HTMLFormElement> = async (event) => {
        event.preventDefault();
        const posts = await getPostsBySearch(search);
        onSearch(posts);
    }

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="search"
                placeholder='search'
                value={search}
                onChange={(event) => setSearch(event.target.value)}
            />
            <button className={styles.button} type="submit">
                Search
            </button>
        </form>
    )
}

export {PostSearch}