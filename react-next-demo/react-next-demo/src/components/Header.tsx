import Link from "next/link";
import React from "react";
import {Navigation} from "@/components/Navigation";

export default function Header() {
    const navItems = [
        {label: "Main", href: "/"},
        {label: "About", href: "/about"},
        {label: "Users", href: "/users"},
        {label: "Blog", href: "/blog"},
        {label: "Catalog", href: "/catalog"},
    ]
    return (
        <header>
            <Navigation navLinks={navItems} />
        </header>
    )
}